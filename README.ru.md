# Webpack v4
---

##### Project builder, webpack v4.
Точка входа **index.(dev|prod).js** в директории **./src** .

# Быстрый старт
---
Для начала работы необходимо установить зависимости из **package.json**
```sh
$ cd webpack-v4
$ npm install
```
И запустить сервер
```sh
$ npm run dev
```
# Скрипты в **package.json**

``` javascript
"scripts": {
    "dev": "webpack-dev-server  --hot --port 3000 --open --mode development",
    "build": "webpack --mode development",
    "prod": "webpack --mode production",
    "clean": "rmdir dist /s/q",
    "lint": "eslint --ext .js,.jsx src && stylelint ./src/**/*.scss --syntax scss",
    "lint:js": "eslint --ext .js,.jsx src",
    "lint:scss": "stylelint ./src/**/*.scss --syntax scss"
  }
```
  >dev - запускает webpack dev server, с mode = development.
  
  >build - собирает проект c mode = development, собранный проект в директории dist.
  
  >prod - собирает проект с mode = poduction, собранный проект в директории dist.
  
  >clean - удаляет директории dits (без подтверждения).
  
  >lint - запускает линтеры для .js и .scss файлов.
  
  >lint:js - запускает линтер для .js файлов.
 
  >lint:scss - запускает линтер для .scss файлов.
  
# Конфигурация **webpack**

 
 Подключение объектов **path** и **webpack**
 
  > const path = require('path');
  > const webpack = require('webpack'); 
    
Подключение плагинов
  > const MiniCssExtractPlugin = require('mini-css-extract-plugin');
  > const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
  > const HtmlWebpackPlugin = require('html-webpack-plugin');
  > const StylelintWebpackPlugin = require('stylelint-webpack-plugin');

- [MiniCssExtractPlugin](https://webpack.js.org/plugins/mini-css-extract-plugin/) - извлекает CSS в отдельный файл.  
- [OptimizeCSSAssetsPlugin](https://github.com/NMFR/optimize-css-assets-webpack-plugin) - оптимизирует CSS файлы (минифицирует).  
- [HtmlWebpackPlugin](https://webpack.js.org/plugins/html-webpack-plugin/) - создает HTML файл, как свой с подключенным js файлом, так и по готовому габлону.  
- [StylelintWebpackPlugin](https://webpack.js.org/plugins/stylelint-webpack-plugin/) - Плагин sltylelint для webpack.  

Булева переменная isDevMode.  

```
isDevMode = true, если mode = development  

isDevMode = false, если mode = production  

```
### Начало работы

``` javascript
    context: path.join(__dirname, 'src'),

    entry: './index',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.js',
    },

    resolve: {
      modules: [path.join(__dirname, 'src'), 'node_modules'],
      extensions: ['.js', '.jsx', '.scss'],
    },
```

> **context** - Указывается абсолютный путь, указывающий webpack на точку входа.

> **entry** - Точка входа в приложение.

> **output** - набор параметров указывающий webpack как и где он должен выводить файлы.
> **path** - абсолютный путь до выходной директории
> **filename** - имя выходного файла.

> **resolve** - Параметр изменяющий способ разрешение модулей.
> :modules - пути в которых необходиме применить данные правила.
> :extensions - какие расширения файлов разрешить.
> Это правило позваляет опустить расширение файла при импорте компонента
> (применяется к файлам с именем "index" )
  
  До разрешения.
  ``` javascript
  import Component from 'components/index.js'
  ```
  После 
  ``` javascript
  import Component from 'components'
  ```
  
# Модули правил
 1. Для .js|.jsx
``` javascript 
 {
  isDevMode ? {
          enforce: 'pre',
          test: /\.(js|jsx)?$/,
          exclude: '/node_modules/',
          loader: 'eslint-loader',
        } : {},
        {
          test: /\.(js|jsx)?$/,
          exclude: '/node_modules/',
          use: {
            loader: 'babel-loader',
          },
        }
 }
```
 Для файлов **.js|.jsx** применяется babel-loader, его настройки лежят в файле .babelrc в корне проекта.  
 Используется для транспиляции ES6 | JSX кода в понятный браузеру ES5.  
 В **dev** режиме используется eslint-loader проверки исходных файлов не измененных другими загрузчиками (например, babel-loader).  
 `enforce: 'pre` используется для применения eslint-loader до применения других загрузчиков.  


2. Для .scss

``` javascript
 {
          test: /\.(sa|sc|c)ss$/,
          use: [
            isDevMode ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader'
          ],
        },
 }
```
Срабатывают поочереди, справа на лево   
 - sass-loader - загружает файл Sass / SCSS и компилирует его в CSS.  
 - postcss-loader - имеет различные настройки, в данном случае работает как autoprefixer.  
 - css-loader - интерпретируют `@import и url()` как `import/require()`.  
- style-loader - Добавляет CSS в DOM, вставляя <style> тег.  
- MiniCssExtractPlugin.loader - плагин извлекает CSS в отдельный файл. (для production режима).  


---

``` javascript
"husky": {
    "hooks": {
      "pre-commit": "lint-staged",
      "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
    }
  },
  "lint-staged": {
    "*.{js,jsx}": [
      "eslint --ext .js,.jsx src",
      "git add"
    ],
    "*.{scss,sass}": [
      "stylelint ./src/**/*.scss --syntax scss",
      "git add"
    ]
  },
```

[Husky](https://github.com/typicode/husky) - говорит, что надо сделать перед коммитом / пушем.
[Lint-staged](https://github.com/okonet/lint-staged) - запускает необходимые линтеры для конкретных файлов.
---
``` javascript
    "@commitlint/cli": "^7.3.2",
    "@commitlint/config-conventional": "^7.3.1",
```
[Commitlint](https://github.com/conventional-changelog/commitlint) - Предоставляет вариант придерживания единого стяли коммитов, заставляя придерживаться его.
