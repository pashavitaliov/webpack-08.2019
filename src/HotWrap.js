import React from 'react';
import { hot } from 'react-hot-loader';
import AdminRouter from 'src/containers/AdminRouter/index';
// import Counter from 'components/Counter';
// import Dog from 'components/Dog';
// import DogFetch from 'Dog/index.fetch';


const HotWrap = () => (
  <div>
    {/* <Counter /> */}
    {/* <Dog /> */}
    <AdminRouter />
    {/* <DogFetch /> */}
  </div>
);
export default hot(module)(HotWrap);
