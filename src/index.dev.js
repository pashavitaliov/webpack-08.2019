import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
// import logger from 'redux-logger';

import { createBrowserHistory } from 'history';
import { routerMiddleware, ConnectedRouter } from 'connected-react-router';

import AdminRouter from 'containers/AdminRouter/index';
import reducer from './rootReducer';
import rootSaga from './rootSaga';
// import HotWrap from './HotWrap';

const history = createBrowserHistory();
const router = routerMiddleware(history);

import './assets/css/styl';

const saga = createSagaMiddleware();

const store = createStore(reducer(history), composeWithDevTools(applyMiddleware(router, saga)));
saga.run(rootSaga);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      {/* <HotWrap /> */}
      <AdminRouter />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
);
