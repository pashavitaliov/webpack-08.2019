import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { routerMiddleware, ConnectedRouter } from 'connected-react-router';

import AdminRouter from 'containers/AdminRouter/index';
import reducer from './rootReducer';
import rootSaga from './rootSaga';

import './assets/css/styl';

const history = createBrowserHistory();
const router = routerMiddleware(history);
const saga = createSagaMiddleware();
const store = createStore(reducer(history), applyMiddleware(router, saga));

saga.run(rootSaga);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <AdminRouter />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
);
