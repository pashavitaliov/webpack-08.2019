import React, { Component } from 'react';
// import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';

@withRouter
class Menu extends Component {
  testRoute = () => {
    const { history } = this.props;
    history.push('/dog');
  };

  render() {
    const { menu } = this.props;

    return (
      <ul className="menu">
        {menu.map(link => (
          <li className="menu__link-wrap" key={link.name}>
            <NavLink
              className="menu__link"
              to={link.path}
              exact
              activeStyle={{ textDecoration: 'underline' }}
            >
              {link.name}
            </NavLink>
          </li>
        ))}
        <li>
          <span onClick={this.testRoute} role="button" tabIndex="0">test dog</span>
        </li>
      </ul>
    );
  }
}

export default Menu;
