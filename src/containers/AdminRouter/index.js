/* eslint-disable */
// import React, { Component } from 'react';
// import { connect } from 'react-redux';
// // import { Router, Route, IndexRoute, browserHistory } from 'react-router';
// import { withRouter, Route, Switch, Redirect, Link } from 'react-router-dom';
//
// class App extends Component {
//   render() {
//     return (
//         <div className='container'>
//           <h1>App</h1>
//           <ul>
//             <li><Link to='/admin'>Admin</Link></li>
//             <li><Link to='/genre'>Genre</Link></li>
//           </ul>
//           {/* добавили вывод потомков */}
//           {this.props.children}
//         </div>
//     )
//   }
// }
//
// const Home = ({children}) => (
//     <div>
//       <h2>Home</h2>
//       {children}
//     </div>
// );
//
// const About = () => (
//     <div>
//       <h2>About</h2>
//     </div>
// );
//
// class AdminRouter extends Component {
//   render() {
//     console.log(this.props);
//     return (
//         <Switch>
//             <Redirect component={About} />
//             <Route path='admin' component={<div>Admin</div>} />
//             <Route path='genre' component={Home} />
//         </Switch>
//     );
//   }
// }
//
//
// export default withRouter(connect()(AdminRouter));


import React, { Component } from "react";
import { connect } from 'react-redux';
import { BrowserRouter as Router, withRouter, Route, Switch, Redirect, NavLink, Link } from 'react-router-dom';
import Menu from './menu';
import Counter from 'components/Counter';
import Dog from 'components/Dog';

const menu = [
  {
    path: '/',
    name: 'main'
  },
  {
    path: '/counter',
    name: 'counter'
  },
  {
    path: '/dog',
    name: 'dog'
  }
];


const AdminRouter = () => (
  <Router>
    <div>
      <Menu menu={menu} />
      <Route exact path="/" component={() => <div>main</div>} />
      <Route path="/counter" component={Counter} />
      <Route path="/dog" component={Dog} />
    </div>
  </Router>
);


export default AdminRouter;