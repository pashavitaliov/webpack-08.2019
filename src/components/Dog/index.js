/* eslint-disable */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/Button';
import './styles.scss';

class Dog extends Component {

  test = () => {
    let p1 = new Promise((resolve, reject) => {
      setTimeout(resolve, 1000, "one");
    });
    let p2 = new Promise((resolve, reject) => {
      setTimeout(resolve, 2000, "two");
    });
    let p3 = new Promise((resolve, reject) => {
      setTimeout(resolve, 3000, "three");
    });
    let p4 = new Promise((resolve, reject) => {
      setTimeout(resolve, 4000, "four");
    });
    let p5 = new Promise((resolve, reject) => {
      setTimeout(resolve, 5000, "five");
    });

    const allPromise = promises => {
      let results = [];

      return new Promise((resolve, reject) => {
        promises.forEach((promise, i) => {
          promise.then(res => {
            results.push(res);
            if (i === promises.length - 1) {
              resolve(results);
            }
          }).catch(error => reject(error));
        })
      })
    };

    allPromise([p1, p2, p3, p4, p5]).then(val => console.log(val));

  };

  render() {
    const { dogReducer: { url, loading }, requestDog } = this.props;

    return (
      <div className="a-wrap dog">
        <Button onClick={requestDog} loading={loading} />
        {/*<button type="button" onClick={requestDog}>*/}
        {/* <button type="button" onClick={this.test}> */}

        {/*{ loading ? 'Loading...' : 'Loading' }*/}
        {/*</button>*/}
        <div className="img-container"><img src={url} alt="" /></div>
      </div>
    );
  }
}

import { ACTIONS } from './reducer';

export default connect(
  state => ({
    dogReducer: state.dogReducer,
  }),
  {
    requestDog: ACTIONS.requestDog,
    requestDogSucceeded: ACTIONS.requestDogSucceeded,
    requestDogFailed: ACTIONS.requestDogFailed
  }
)(Dog);

// dispatch => ({
//   requestDog: () => dispatch(ACTIONS.requestDog()),
//   requestDogSucceeded: (data) => dispatch(ACTIONS.requestDogSucceeded(data)),
//   requestDogFailed: () => dispatch(ACTIONS.requestDogFailed())
// })
