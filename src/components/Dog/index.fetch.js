import React, { Component } from 'react';
import { connect } from 'react-redux';

import './styles.scss';

class Dog extends Component {
  fetchDog = () => {
    const { requestDog, requestDogSucceeded, requestDogFailed } = this.props;
    requestDog();
    return fetch('https://dog.ceo/api/breeds/image/random')
      .then(res => res.json())
      .then((result) => {
        requestDogSucceeded(result);
        return result;
      })
      .then(result => (console.log(result)))
      .catch(requestDogFailed());
  };

  render() {
    const { dogReducer: { url, loading } } = this.props;

    return (
      <div className="dog">
        <button type="button" onClick={this.fetchDog}>
          {loading ? 'LoadingF...' : 'LoadingF'}
        </button>
        <div className="img-container"><img src={url} alt="" /></div>
      </div>
    );
  }
}

import { ACTIONS } from './reducer';

export default connect(
  state => ({
    dogReducer: state.dogReducer,
  }),
  {
    requestDog: ACTIONS.requestDog,
    requestDogSucceeded: ACTIONS.requestDogSucceeded,
    requestDogFailed: ACTIONS.requestDogFailed
  }
)(Dog);

// dispatch => ({
//   requestDog: () => dispatch(ACTIONS.requestDog()),
//   requestDogSucceeded: (data) => dispatch(ACTIONS.requestDogSucceeded(data)),
//   requestDogFailed: () => dispatch(ACTIONS.requestDogFailed())
// })
