import React from 'react';
import './styles.scss';

const Button = ({ onClick, loading }) => {
    return (
      <button type="button" onClick={onClick}>
        {
          loading
            ? (
              <div className="lds-ripple">
                <div />
                <div />
              </div>
            )
            : 'request'
        }
      </button>
    );
};

export default Button;
