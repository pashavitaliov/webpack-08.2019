import React from 'react';
import { connect } from 'react-redux';
import './styles.scss';

const TestApp = ({ counterReducer: { count }, plus, minus }) => (
  <div className="a-wrap counter wrapper">
    <button type="button" onClick={plus}>+</button>
    <button type="button" onClick={minus}>-</button>
    <div>{count}</div>
  </div>
);

import { ACTIONS } from './reducer';

export default connect(
  state => (
    state
  ),
  {
    plus: ACTIONS.plus,
    minus: ACTIONS.minus,
  },
)(TestApp);

// const mapStateToProps = state => ({
//   testReducer: state.testReducer
// });
// const mapDispatchToProps = dispatch => ({
//   plus: () => dispatch(ACTIONS.plus()),
//   minus: () => dispatch(ACTIONS.minus())
// });
//
// export default connect(mapStateToProps, mapDispatchToProps)(TestApp);
