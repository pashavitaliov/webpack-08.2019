export const TYPES = {
  PLUS: 'PLUS',
  MINUS: 'MINUS',
};

const initialState = {
  count: 0,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPES.PLUS: {
      return {
        count: state.count + 1
      };
    }

    case TYPES.MINUS: {
      return {
        count: state.count - 1
      };
    }

    default:
      return state;
  }
};

export const ACTIONS = {
  plus: () => ({ type: TYPES.PLUS }),
  minus: () => ({ type: TYPES.MINUS })
};
