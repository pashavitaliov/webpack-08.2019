import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as counter } from './components/Counter/reducer';
import { reducer as dog } from './components/Dog/reducer';

export default history => combineReducers({
  router: connectRouter(history),
  counterReducer: counter,
  dogReducer: dog
});
